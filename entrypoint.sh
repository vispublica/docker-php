#!/bin/bash

set -x

# Start PHP built-in
su kratos -c "cd /app && php -S 0.0.0.0:5000 -t public"
